package terraform.octobarnacle

import future.keywords.in
import input as tfplan

# Each rule passed is one score. There are 20 rds rules
total_score_rds = 20

# Each rule passed is one score. There are 10 VPC rules
total_score_VPC = 10

# Minimum score for validation to pass
expected_score = total_score_rds + total_score_VPC

analysis[results] {
	# Run through all defined rules
	rules := [
		rule_rds_cluster_data_encryption,
		rule_rds_cluster_deployed_in_multi_az,
		rule_rds_cluster_iam_enabled,
		rule_rds_cluster_automated_backup,
		rule_rds_cluster_deletion_protection,
		rule_rds_cluster_backtracking,
		rule_rds_cluster_backup_plan,
		rule_rds_cluster_automatic_minor_version_upgrades,
		rule_vpc_inbound_private_subnets,
		rule_vpc_inbound_public_subnets,
		rule_vpc_high_availability,
		rule_vpc_subnets_capacity,
		rule_vpc_security_group_ports_accessible,
	]

	# Evalute how many are true and how many are false
	policy_passed := count([res | res := rules[_]; contains(res, ":true")])
	policy_failed := count([res | res := rules[_]; contains(res, ":false")])

	# Output result. Overall valid if the expected_score is equal to total policy_passed
	results := {
		"expected_score": expected_score,
		"total_polices": sum([policy_passed, policy_failed]),
		"policy_passed": policy_passed,
		"policy_failed": policy_failed,
		"validated": expected_score == policy_passed,
		"results": rules,
	}
}

#-----
# Rules to validate RDS resource based on policy requirments
# Rules are either true or false. This is added to the output and evaluated in
# the analysis function
#-----

#RDS1 I confirm no snapshots are created with public access TODO: review rule
#rule_snapshots_with_public_access = msg {
#	# loop though and returns only resource specified
#	resource := resources.aws_rds_cluster[i]
#
#	# resource will be the object specified above
#	# carryout validations
#	# if ok increment current_score
#	# add error message to output else ok message
#	msg = sprintf("%v :%v", [resource.type, true])
#}

#RDS2 Are the RDS instances and snapshots created by the solution enabled with data encryption?
rule_rds_cluster_data_encryption = msg {
	resource := resources.aws_rds_cluster[i]
	storage_encrypted := resource.change.after.storage_encrypted
	msg := sprintf("%v has been enabled with data encryption :%v", [resource.type, storage_encrypted])
}

#RDS3 Is the RDS cluster deployed in a Multi-AZ configuration?
rule_rds_cluster_deployed_in_multi_az = msg {
	resource := resources.aws_rds_cluster[i]
	availability_zones := object.get(resource.change.after_unknown, "availability_zones", false)
	msg := sprintf("%v was deployed to multi az :%v", [resource.type, availability_zones])
}

#RDS6 Is the IAM Database Authentication feature enabled for all RDS PostgresSQL
rule_rds_cluster_iam_enabled = msg {
	resource := resources.aws_rds_cluster[i]
	iam_db := object.get(resource.change.after, "iam_database_authentication_enabled", false)
	msg := sprintf("%v iam database authentication feature enabled :%v", [resource.type, iam_db])
}

#RDS10	Do the RDS instances have Deletion Protection feature enabled
rule_rds_cluster_deletion_protection = msg {
	resource := resources.aws_rds_cluster[i]
	deletion_protection := object.get(resource.change.after, "deletion_protection", false)
	msg := sprintf("%v deletion protection feature enabled :%v", [resource.type, deletion_protection == true])
}

#RDS13	Are the RDS instances configured for automated backup?
rule_rds_cluster_automated_backup = msg {
	resource := resources.aws_rds_cluster[i]
	retention_period := object.get(resource.change.after, "backup_retention_period", 0)
	msg := sprintf("%v instances configured for automated backup :%v", [resource.type, retention_period >= 1])
}

#RDS16   Are Amazon Aurora DB clusters protected by a backup plan
rule_rds_cluster_backup_plan = msg {
	all := resources.aws_rds_cluster_instance
	cluster_instances := [res | res := all[_]; object.get(res.change.after_unknown, "preferred_backup_window", {}) == false]
	msg := sprintf("%v protected by a backup plan :%v", ["aws_rds_cluster_instance", count(cluster_instances) == 0])
}

#RDS17	Does the Amazon Aurora MySQL cluster have backtracking enabled
rule_rds_cluster_backtracking = msg {
	resource := resources.aws_rds_cluster[i]
	backtracking := object.get(resource.change.after, "backtrack_window", 0)
	msg := sprintf("%v clusters have backtracking enabled :%v", [resource.type, backtracking > 0])
}

#RDS18	Checks if Amazon Relational Database Service (RDS) database instances are configured for automatic minor version upgrades
rule_rds_cluster_automatic_minor_version_upgrades = msg {
	all := resources.aws_rds_cluster_instance
	cluster_instances := [res | res := all[_]; res.change.after.auto_minor_version_upgrade == false]
	msg := sprintf("%v all database instances configured for automatic minor version upgrades :%v", ["aws_rds_cluster_instance", count(cluster_instances) < 1])
}

#----- RDS rules end

#-----
# Rules to validate VPC resource based on policy requirements
# Rules are either true or false. This is added to the output and evaluated in
# the analysis function
#-----

#VPC1:	Are the availability zones maximized for high availability and disaster recovery?
rule_vpc_high_availability = msg {
	subnets := resources.aws_subnet

	# get private subnets availability zones
	public_subnets = [res | res := subnets[_]; res.change.after.map_public_ip_on_launch == false]
	availability_zones = [res | res := public_subnets[_].change.after.availability_zone; res != ""]

	msg := sprintf("%v private subnets in multiple avilability zones :%v", ["aws_subnet", count(availability_zones) > 2]) #TODO: find unique list
}

#VPC2: (a)	Does the solution implement separate subnets for unique routing requirements?
#For example, public subnets for external-facing resources and private subnets for internal resources

rule_vpc_inbound_private_subnets = msg {
	# get aws_security_group_rule cidr ip array
	all := resources.aws_security_group_rule
	security_groups := [res | res := all[_]; count(res.change.after.cidr_blocks) > 0]
	cidr_blocks := security_groups[_].change.after.cidr_blocks

	# get all aws_subnet where cidr matches
	subnets := [res | res := resources.aws_subnet[_]; res.change.after.cidr_block == cidr_blocks[j]]

	# check if "map_public_ip_on_launch": false
	private_subnets = [res | res := subnets[_]; res.change.after.map_public_ip_on_launch == false]

	# should have at least one private subnet for rule to be true
	msg := sprintf("%v %v private subnet(s) with attached security policy :%v", ["aws_security_group_rule", count(private_subnets), count(private_subnets) > 0])
}

#VPC2: (b)
rule_vpc_inbound_public_subnets = msg {
	# get aws_security_group_rule cidr ip array
	all := resources.aws_security_group_rule
	security_groups := [res | res := all[_]; count(res.change.after.cidr_blocks) > 0]
	cidr_blocks := security_groups[_].change.after.cidr_blocks

	# get all aws_subnet where cidr matches
	subnets := [res | res := resources.aws_subnet[_]; res.change.after.cidr_block == cidr_blocks[j]]

	# check if "map_public_ip_on_launch": true
	public_subnets = [res | res := subnets[_]; res.change.after.map_public_ip_on_launch == true]

	# should have no public subnet allowing traffic on security rule for rule to be true
	msg := sprintf("%v %v public subnet(s) with attached security policy :%v", ["aws_security_group_rule", count(public_subnets), count(public_subnets) == 0])
}

#VPC6: Does the solution's VPC support capacity for additional subnets
rule_vpc_subnets_capacity = msg {
	# get all vpc's  should be one in this case
	all := resources.aws_vpc

	# get all vpc cidr ranges (only one in this policy so using 0 index)
	vpcs_with_cidr := [res | res := all[_]; contains(res.change.after.cidr_block, "/")]
	cidrs := vpcs_with_cidr[0].change.after.cidr_block

	# find the cidr range by copping off everything before /
	start_index := indexof(cidrs, "/") + 1
	end_index := count(cidrs)

	# convert string to number
	range := to_number(substring(cidrs, start_index, end_index))

	# should have have at least a /18 which has 16,384 ips which is about 64 subnets if subnets are /24
	msg := sprintf("%v has wide enough CIDR range for subnets :%v", ["aws_vpc", range <= 18])
}

# VPC9:	Do any security groups with inbound 0.0.0.0/0 have TCP or UDP ports accessible
rule_vpc_security_group_ports_accessible = msg {
	# get all vpc's  security groups
	all := resources.aws_security_group

	# get all security groups that have '0.0.0.0/0' cidr ips
	aws_security_groups := [res | res := all[_]; "0.0.0.0/0" == res.change.after.ingress.cidr_blocks[i]]

	# get all security groups with ingress ::/0 that have a port assigned
	ports := [res | res := aws_security_groups[_]; to_number(res.change.after.ingress.to_port) > 0]

	#should not have any open ingress ports, count should be zero
	msg := sprintf("%v security groups with 0.0.0.0/0 have no accessible tcp/udp ports :%v", ["aws_security_group", count(ports) == 0])
}

#------ VPC rules end

# Utility functions to parse json input
#------

# list of all resources of a given type
resources[resource_type] = all {
	resource_type := resources_types[r].type
	all := [name |
		name := tfplan.resource_changes[_]
		name.type == resource_type
	]
}

# Gets all resources on json plan
resources_types := {r |
	some path, value

	# Walk over the JSON tree and check if the node we are
	# currently on is a module (either root or child) resources
	# value.
	walk(input.planned_values, [path, value])

	# Look for resources in the current value based on path
	rs := module_resources(path, value)

	# Aggregate them into `resources`
	r := rs[_]
}

# Variant to match root_module resources
module_resources(path, value) = rs {
	reverse_index(path, 1) == "resources"
	reverse_index(path, 2) == "root_module"
	rs := value
}

# Variant to match child_modules resources
module_resources(path, value) = rs {
	reverse_index(path, 1) == "resources"
	reverse_index(path, 3) == "child_modules"
	rs := value
}

reverse_index(path, idx) = value {
	value := path[minus(count(path), idx)]
}
