# symmetrical-octo-barnacle
Collabo

Run with exit code defined
```bash
opa eval --format pretty --fail-defined -d policy-rds.rego -i ./plan.json "data.terraform.octobarnacle.analysis"
```

Exit code can be used to fail pipeline
```bash
echo $?
```

## Implementation Ideas

### Policy as a Service
The idea is to run OPA as a service. <br>
We could spin up a Docker container that has OPA backed into it and running the [OPA server](https://www.openpolicyagent.org/docs/latest/#4-try-opa-run-server)
A client can now call the API passing in the Terraform JSON plan. The output can now be parsed and a decision taken

#####Steps:
- Create a docker container with [OPA server](https://www.openpolicyagent.org/docs/latest/#4-try-opa-run-server) running
- Host the service on Heroku or some other location or AWS etc
- Create a pipeline in Travis, Circle CI or BitBucket
  - First step [converts the Terraform plan into JSON](https://www.openpolicyagent.org/docs/v0.21.1/terraform/#2-convert-the-terraform-plan-into-json)
  - Next step make an API call using `curl` or `wget` to the OPA service above
  - Next step of the pipeline takes a desision to fail the build if validation is false
  - Can also send out email or even trigger an AWS lambda funtion and more fun stuff can be done from AWS

Examples of using the OPA server can be found in the [OPA run](https://www.openpolicyagent.org/docs/latest/#4-try-opa-run-server) section of the documentation

### CI/CD Policy validation
The idea is to have a regular CI/CD build pipeline<br>
We could use BitBucket, Travis CI or Circle CI<br>
The pipeline would run and fail the build if the OPA validation is false

#####Steps:
- Create a docker container with [OPA server](https://www.openpolicyagent.org/docs/latest/#4-try-opa-run-server) running
- Create a pipeline
  - First step [converts the Terraform plan into JSON](https://www.openpolicyagent.org/docs/v0.21.1/terraform/#2-convert-the-terraform-plan-into-json) output to a file location
  - Call the OPA command above via bash script 
    ```bash 
       result=$(opa eval --format pretty --fail-defined -d policy-rds.rego -i ./plan.json "data.terraform.octobarnacle.analysis")
    ```
    The output will be stored in the `$result` variable
  - Next step is to read that variable and if `validate` is false then fail the build and output `$result` to console

