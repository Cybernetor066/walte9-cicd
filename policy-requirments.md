## Policies For RDS Aurora postgresql

 - RDS1:	I confirm no snapshots are created with public access
 - [x] RDS2:	Are the RDS instances and snapshots created by the solution enabled with data encryption?
 - [x] RDS3:	Is the RDS cluster deployed in a Multi-AZ configuration?
 - RDS4:	Are the solution components that communicate with an RDS instance using SSL/TLS for encryption in transit?
 - RDS5:	Are security credentials for accessing the RDBMS are stored in AWS Secrets manager with automatic rotation enabled
 - [x] RDS6:	Is the IAM Database Authentication feature enabled for all RDS PostgresSQL
 - RDS7:	Does the solution's DB security groups follow outlined best practices for access and not publicly accessible?
 - RDS8:	Is access to the RDS instance from 0.0.0.0/0 is disabled
 - RDS9:	Is the RDS instance created in a private subnet within a VPC?
 - [x] RDS10:	Do the RDS instances have Deletion Protection feature enabled?
 - RDS11:	Are the RDS instances using the default endpoint ports
 - RDS12:	Is RDS resource event notification enabled and subscribed to?
 - [x] RDS13:	Are the RDS instances configured for automated backup?
 - RDS14:	Is the Backtrack feature is enabled for all Amazon Aurora clusters? (repetition, covered in `RDS17`)
 - [x] RDS15:	If using Amazon Aurora, is the Deletion Protection feature enabled for the cluster? (repetition, covered in `RDS10`)
 - [x] RDS16:   Are Amazon Aurora DB clusters protected by a backup plan
 - [x] RDS17:	Does the Amazon Aurora MySQL cluster have backtracking enabled
 - [x] RDS18:	Checks if Amazon Relational Database Service (RDS) database instances are configured for automatic minor version upgrades
 - RDS19:	Is enhanced monitoring enabled for Amazon Relational Database Service (Amazon RDS) instances.
 - RDS20:	Are log types exported to Amazon CloudWatch for an Amazon Relational Database Service (Amazon RDS) instance are enabled


##Policies For VPC

- [x] VPC1:	Are the availability zones maximized for high availability and disaster recovery?
- [x] VPC2a: Does the solution implement separate subnets for unique routing requirements? For example, public subnets for external-facing resources
- [x] VPC2b: Does the solution implement separate subnets for unique routing requirements? For example, private subnets for internal resources
- VPC3:	Are there any unused network access control lists (network ACLs)?
- VPC4:	Does the solution use independent routing tables configured for every private subnet?
- VPC5:	Does the solution use NAT Gateways instead of NAT instances in supporting regions?
- [x] VPC6:	Does the solution's VPC support capacity for additional subnets?
- VPC7:	Does the solution have VPC Flow Logs enabled?
- VPC8:	Does the default security group of any Amazon Virtual Private Cloud (VPC) allow inbound or outbound traffic?
- [x] VPC9:	Do any security groups with inbound 0.0.0.0/0 have TCP or UDP ports accessible

## Understanding the Terraform json structure
[JSON Output Format](https://www.terraform.io/docs/internals/json-format.html#configuration-representation)